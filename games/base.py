from random import shuffle


class Game:
    IN_GAME = 0
    LOST = -1
    WON = 1

    def __init__(self):
        self.player_fields = list()


class CardDeck:
    class Card:
        def __init__(self, value, suit):
            self.value = value
            self.suit = suit

        def __repr__(self):
            suits = {
                'hearts': u"\u2665",
                'clubs': u"\u2663",
                'diamonds': u"\u2666",
                'spades': u"\u2660"
            }
            return f"{self.value}{suits[self.suit]}"

    def __init__(self):
        values = ['2', '3', '4', '5', '6', '7', '8',
                  '9', '10', 'J', 'Q', 'K', 'A']
        suits = ['hearts', 'spades', 'clubs', 'diamonds']
        self.cards = list()
        for v in values:
            for s in suits:
                self.cards.append(self.Card(v, s))
        shuffle(self.cards)
        self.card = self._get_next_card()
        self.reshuffles = 0

    def shuffle_deck(self):
        # actual reshuffling the deck
        shuffle(self.cards)
        # reset generator to start picking cards from the beginning
        self.card = self._get_next_card()
        self.reshuffles += 1

    def _get_next_card(self):
        for i in range(len(self.cards)):
            yield self.cards[i]
