from logging import getLogger

from core.bot import TelegramBot
from core.user import UserSession
from games.base import Game

log = getLogger('lodumanych')


class Roulette(Game):
    class Table:
        def __init__(self):
            pass

    def __init__(self):
        self.player_fields = ['status', 'stake']

    @classmethod
    def play(cls, call, session: UserSession, telegram: TelegramBot):
        chat_id = call.from_user.id
        message_id = call.message.message_id
        call_data = call.data
        log.debug(f'ROULETTE: User {chat_id} pressed {call_data} '
                  f'in message {message_id}')

    @classmethod
    def start_new_game(cls, session: UserSession):
        if session.balance > 50:
            session.roulette['status'] = cls.IN_GAME
            session.state = 'roulette'
            log.debug(
                f'ROULETTE: Starting new game for user {session.user_id}.')
            return cls.generate_menu({
                'status': session.roulette['status'],
                'stake': session.roulette['stake']
            })
        else:
            log.debug(
                f'ROULETTE: Cannot start game for user {session.user_id}. '
                f'Not enough pts: {session.balance}.')
            return cls.generate_menu({
                'status': 9,
                'error': 
                    'You have insufficient amount on your balance to play:('
            })

    @classmethod
    def generate_menu(cls, data: dict):
        pass
