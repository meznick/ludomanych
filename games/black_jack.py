from logging import getLogger

from telebot import AsyncTeleBot

from core.bot import TelegramBot
from core.menu import InlineMenu, MainMenu
from core.user import UserSession
from games.base import CardDeck, Game

log = getLogger('ludomanych')


class BlackJackGame(Game):
    def __init__(self):
        Game.__init__(self)
        self.player_fields = ['hand', 'score', 'deck', 'status']

    @classmethod
    def play(cls, call, session: UserSession, telegram: TelegramBot):
        chat_id = call.from_user.id
        message_id = call.message.message_id
        call_data = call.data
        log.debug(f'BJACK: User {chat_id} pressed {call_data} '
                  f'in message {message_id}')
        if call_data == "take":
            menu = cls.take_card(session)
        elif call_data == 'new_game':
            menu = cls.start_new_game(session)
        elif call_data == 'stay':
            menu = cls.finish_round(session)
        elif call_data == 'exit':
            menu = cls.exit_to_main_menu(session)
            menu.title = menu.title.format(user=session)
            telegram.user_data.update_balance(session.user_id, session.balance)
        else:
            log.debug(f'BJACK: Unknown callback <{call_data}>...')
            return 1
        return telegram.bot.edit_message_text(
            text=menu.title,
            chat_id=chat_id,
            message_id=message_id,
            reply_markup=menu.keyboard
        )

    @classmethod
    def start_new_game(cls, session: UserSession):
        if session.balance >= 50:
            deck = session.blackjack['deck']
            if deck is None:
                deck = CardDeck()
                session.blackjack['deck'] = deck
            else:
                deck.shuffle_deck()
            new_hand = [next(deck.card) for _ in range(2)]
            score = cls.calculate_score(new_hand)
            status = cls.IN_GAME if score != 21 else cls.WON
            session.blackjack['hand'] = new_hand
            session.blackjack['score'] = score
            session.blackjack['status'] = status
            session.state = 'blackjack'
            log.debug(
                f'BJACK: Starting new game for user {session.user_id}. '
                f'New hand: {new_hand}')
            return cls.generate_menu({
                'hand': new_hand,
                'status': status
            })
        else:
            log.debug(
                f'BJACK: Cannot start game for user {session.user_id}. '
                f'Not enough pts: {session.balance}.')
            return cls.generate_menu({
                'hand': [],
                'status': 9,
                'error': 'You have insufficient amount on your balance to play:('
            })

    @classmethod
    def take_card(cls, session: UserSession):
        hand = session.blackjack['hand']
        deck = session.blackjack['deck']
        hand.append(next(deck.card))
        session.blackjack['score'] = cls.calculate_score(hand)
        status = cls.IN_GAME
        log.debug(f'BJACK: gave new card to {session.user_id}. '
                  f'Hand now: {hand}')
        if session.blackjack['score'] > 21:
            status = cls.LOST
            session.balance -= 50
            log.debug(f'BJACK: user {session.user_id} lost. '
                      f'New balance: {session.balance}')
        elif session.blackjack['score'] == 21:
            status = cls.WON
            session.balance += 75
            log.debug(f'BJACK: user {session.user_id} won. '
                      f'New balance: {session.balance}')
        session.blackjack['status'] = status
        return cls.generate_menu({
            'hand': hand,
            'status': status
        })

    @classmethod
    def finish_round(cls, session: UserSession):
        hand = session.blackjack['hand']
        deck = session.blackjack['deck']
        user_id = session.user_id
        player_score = cls.calculate_score(hand)
        bot_hand, bot_score = cls.calculate_ai(deck, player_score)
        if player_score < bot_score < 22:
            session.blackjack['status'] = cls.LOST
            session.balance -= 50
        else:
            session.blackjack['status'] = cls.WON
            session.balance += 75
        log.debug(f"BJACK: Finishing round for {user_id}. "
                  f"New balance: {session.balance}")
        return cls.generate_menu({
            'hand': hand,
            'status': session.blackjack['status'],
            'bot_hand': bot_hand
        })

    @classmethod
    def exit_to_main_menu(cls, session: UserSession):
        session.state = 'main_menu'
        log.debug(f'BJACK: User {session.user_id} exiting to main menu.')
        return cls.generate_menu({
            'status': 9,
            'hand': []
        })

    @classmethod
    def calculate_ai(cls, deck: CardDeck, player_score: int):
        hand = list()
        top = max(16, player_score)
        score = 0
        log.debug(f'BJACK: Calculating bot actions for score: {player_score}')
        while score < top:
            hand.append(next(deck.card))
            score = cls.calculate_score(hand)
        return hand, score

    @classmethod
    def generate_menu(cls, data: dict):
        cards = data['hand']
        status = data['status']
        rendered_cards = [f"{card}" for card in cards]
        try:
            bot_hand = data['bot_hand']
            bot_title = "AI hand: " + ", ".join(
                [f"{c}" for c in bot_hand]) + "\n"
        except KeyError:
            bot_title = ''
        try:
            error_msg = data['error']
        except KeyError:
            error_msg = ''
        title = "Your hand: " + ", ".join(rendered_cards)
        if status == cls.IN_GAME:
            items = [
                {"text": "Take", "query": "take"},
                {"text": "Stay", "query": "stay"}
            ]
        elif status == cls.WON:
            title = "You won 75 pts!\n" + bot_title + title
            items = [
                {"text": "New game", "query": "new_game"},
                {"text": "Exit", "query": "exit"}
            ]
        elif status == cls.LOST:
            title = f"You lost 50 pts:(\n" + bot_title + title
            items = [
                {"text": "New game", "query": "new_game"},
                {"text": "Exit", "query": "exit"}
            ]
        else:
            menu = MainMenu().menus['start_screen']
            menu.title = error_msg + menu.title
            return menu
        return InlineMenu(items=items, title=title)

    @classmethod
    def calculate_score(cls, hand: list):
        """
        Better not to use outside this class and tests.
        """
        amounts = {
            '2': 2,
            '3': 3,
            '4': 4,
            '5': 5,
            '6': 6,
            '7': 7,
            '8': 8,
            '9': 9,
            '10': 10,
            'J': 10,
            'Q': 10,
            'K': 10,
            'A': 0
        }
        _score = 0
        hand = [amounts[card.value] for card in hand]
        hand.sort()
        for c in hand:
            if c != 0:
                _score += c
            else:
                if _score > 10:
                    _score += 1
                else:
                    _score += 11
        return _score
