# -*- coding: utf-8 -*-
import functools
from argparse import ArgumentParser
from logging import getLogger, FileHandler, Formatter, StreamHandler
import json
from os import environ

from core.bot import TelegramBot
from core.menu import MainMenu
from core.user import find_user_session
from games.black_jack import BlackJackGame
from games.roulette import Roulette

parser = ArgumentParser('TELEGRAM')
parser.add_argument('-T', '--telegram', help='Telegram Bot API key.', required=False)
parser.add_argument('-H', '--host', dest='proxy_host',
                    help='Address of proxy host (optional).', required=False)
parser.add_argument('-P', '--port',  dest='proxy_port',
                    help='Port of proxy host (optional).', required=False)
parser.add_argument('-C', dest='proxy_auth',
                    help='Proxy auth credentials formatted as usr:pwd.')
parser.add_argument('-D', dest='database', help='Database server address.')
parser.add_argument('-V', dest='database_auth',
                    help='Database auth credentials formatted as usr:pwd:dbname.')
args = parser.parse_args()


def collect_environment(required: list, optional: list) -> dict:
    """
    Collects environment and arguments required for module to work.
    If some of variables are unavailable raises exception.
    """
    environment = dict()
    for variable in required:
        try:
            environment.update({variable: environ[variable.upper()]})
        except KeyError:
            try:
                environment.update({variable: getattr(args, variable)})
            except AttributeError:
                raise Exception(f'No variable {variable} provided')
    for variable in optional:
        try:
            environment.update({variable: environ[variable.upper()]})
        except KeyError:
            try:
                environment.update({variable: getattr(args, variable)})
            except AttributeError:
                environment.update({variable: None})
    return environment


with open('app.cfg') as config_file:
    configuration = json.load(config_file)


class Application:
    def __init__(self, config: dict, env_variables: dict):
        self.__prepare_logger(config)
        self.telegram = TelegramBot(env_variables)
        self.user_sessions = list()
        self.games = {
            'blackjack': BlackJackGame(),
            'roulette': Roulette()
        }
        self.menu = MainMenu()

    def __prepare_logger(self, config):
        self.log = getLogger('ludomanych')
        self.log.setLevel(config['log_level'])
        fh = FileHandler(config['log_file'], mode='w+', encoding='utf-8')
        sh = StreamHandler()
        fh.setLevel(config['log_level'])
        fm = Formatter(u'%(asctime)s - %(levelname)s - %(message)s')
        fh.setFormatter(fm)
        sh.setFormatter(fm)
        self.log.addHandler(fh)
        self.log.addHandler(sh)
        self.log.info('Logger initiated!')

    def preprocess_decorator(self):
        def decorator_outer(func):
            @functools.wraps(func)
            def decorator(*a, **kw):
                msg = a[0]
                try:
                    text = 'message: ' + msg.text
                except AttributeError:
                    text = 'callback: ' + msg.data
                self.log.debug(f"From {msg.from_user.id} got {text}")
                if msg.from_user.id not in [u.user_id for u in self.user_sessions]:
                    self.telegram.handle_first_login(
                        msg, self.user_sessions, self.games)
                task = func(*a, **kw)
                while not task.done:
                    task.wait()
                self.log.debug(f'Bot responded: {task.result.json}')
                return task
            return decorator
        return decorator_outer

    def start(self):

        @self.telegram.bot.message_handler(commands=['start'])
        @self.preprocess_decorator()
        def handle_messages(msg):
            chat_id = msg.from_user.id
            session = find_user_session(self.user_sessions, chat_id)
            return self.telegram.process_messages(msg, session, self.menu)

        @self.telegram.bot.callback_query_handler(func=lambda x: True)
        @self.preprocess_decorator()
        def handle_games(call):
            chat_id = call.from_user.id
            session = find_user_session(self.user_sessions, chat_id)
            if session.state == 'blackjack':
                response_task = self.games['blackjack'].play(
                    call, session, self.telegram)
            elif session.state == 'main_menu':
                response_task = self.menu.handle_menu(
                    call, session, self.telegram.bot, self.games)
            return response_task

        self.telegram.bot.polling()

    def stop(self):
        self.log.debug('Saving users to database')
        for session in self.user_sessions:
            self.telegram.user_data.update_balance(
                session.user_id, session.balance)


if __name__ == '__main__':
    env = collect_environment(required=[
        'telegram'
    ], optional=[
        'proxy_port', 'proxy_host', 'proxy_auth', 'database', 'database_auth'
    ])
    print(configuration)
    print(env)
    app = Application(configuration, env)
    app.start()
    app.stop()
