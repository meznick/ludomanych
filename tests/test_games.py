from unittest import TestCase, main

from games.base import CardDeck
from games.black_jack import BlackJackGame


class TestCardDeck(TestCase):
    def test_deck_integrity(self):
        deck = CardDeck()
        got_cards = list()
        for i in range(62):
            new_card = next(deck.card)
            with self.subTest('Not in got list.', i=i):
                self.assertNotIn(new_card, got_cards)
                got_cards.append(new_card)
            with self.subTest('In got list.', i=i):
                self.assertIn(new_card, got_cards)
            with self.subTest('In source list.', i=i):
                self.assertIn(new_card, deck.cards)

    def test_reshuffling(self):
        deck = CardDeck()
        with self.subTest('First card'):
            new_card = next(deck.card)
            self.assertEqual(deck.cards[0], new_card)
            next_card_old = deck.cards[1]
        with self.subTest('After reshuffle'):
            deck.shuffle_deck()
            self.assertNotEqual(new_card, deck.cards[0])
            # if reshuffling works, this cards will be different in most cases
            self.assertNotEqual(next(deck.card), next_card_old)


class TestBlackJack(TestCase):
    def test_score_calculation(self):
        game = BlackJackGame()
        for _ in range(100):
            hand, score, deck = game.start_new_game()
            with self.subTest(cards=[c.value for c in hand]):
                self.assertLessEqual(game.calculate_score(hand), 21)
                self.assertEqual(score, game.calculate_score(hand))
        hand = [CardDeck.Card('2', 'hearts'), CardDeck.Card('A', 'hearts')]
        with self.subTest(cards=[c.value for c in hand]):
            self.assertEqual(13, game.calculate_score(hand))


if __name__ == '__main__':
    main()
