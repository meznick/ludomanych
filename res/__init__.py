import os


ROOT_DIR = os.getcwd()
DATABASE_SERVER_CERT = os.path.join(ROOT_DIR, 'res', 'server-ca.pem')
DATABASE_CLIENT_CERT = os.path.join(ROOT_DIR, 'res', 'client-cert.pem')
DATABASE_CLIENT_KEY = os.path.join(ROOT_DIR, 'res', 'client-key.pem')
