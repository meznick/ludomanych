from typing import List, Dict
from logging import getLogger

from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from telebot import AsyncTeleBot

from core.user import UserSession

log = getLogger('ludomanych')


class Menu:
    def __init__(self, items: List[Dict], title: str):
        self.title = title
        self.items = items
        self.keyboard = None

    def _add_keys(self):
        pass


class InlineMenu(Menu):
    def __init__(self, items, title):
        Menu.__init__(self, items, title)
        self.keyboard = InlineKeyboardMarkup()
        self.keyboard.add(*[
            InlineKeyboardButton(text=item['text'],
                                 callback_data=item['query'])
            for item in self.items])


class MainMenu:
    def __init__(self):
        start_screen = InlineMenu(
            title="""
Welcome!

Your profile.
{user}
Main menu""",
            items=[
                {"text": "BlackJack", "query": "blackjack"},
                {"text": "Settings", "query": "settings"},
                {"text": "About", "query": "about"}
            ]
        )
        settings_screen = InlineMenu(
            title="""
Settings.

Language settings are coming soon.
""",
            items=[
                {"text": "Back", "query": "exit"}
            ]
        )
        about_screen = InlineMenu(
            title="""
About.

A casino is simply a public place where a variety of games of chance can be played, and where gambling is the primary activity engaged in by patrons.
""",
            items=[
                {"text": "Back", "query": "exit"}
            ]
        )
        self.menus = {
            'start_screen': start_screen,
            'settings_screen': settings_screen,
            'about_screen': about_screen
        }

    def handle_menu(self, call, session: UserSession,
                    bot: AsyncTeleBot, games: dict):
        chat_id = call.from_user.id
        message_id = call.message.message_id
        call_data = call.data
        log.debug(f'MENU: User {chat_id} pressed {call_data} '
                  f'in message {message_id}')
        if call_data == 'blackjack':
            menu = games['blackjack'].start_new_game(session)
        elif call_data == 'settings':
            menu = self.menus['settings_screen']
        elif call_data == 'about':
            menu = self.menus['about_screen']
        elif call_data == 'exit':
            menu = self.menus['start_screen']
            menu.title = menu.title.format(user=session)
        return bot.edit_message_text(
            text=menu.title,
            chat_id=chat_id,
            message_id=message_id,
            reply_markup=menu.keyboard
        )
