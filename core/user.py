# -*- coding: utf-8 -*-
import functools
from typing import List
from logging import getLogger

import psycopg2
from res import DATABASE_CLIENT_CERT, DATABASE_CLIENT_KEY, \
    DATABASE_SERVER_CERT
from res.translations import user_repr

log = getLogger('ludomanych')


class UserSession:
    """
    Current user's session data.
    """
    def __init__(self, user_id: int, games: list, username: str,
                 first_name: str, last_name: str, language: str,
                 balance: float = 0.0):
        self.user_id = user_id
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.lang = language
        self.state = 'main_menu'
        self.balance = balance
        for game in games:
            self.__setattr__(
                game,
                {field: None for field in games[game].player_fields})

    def __repr__(self):
        first = self.first_name if self.first_name is not None else ''
        last = self.balance if self.last_name is not None else ''
        return user_repr.format(
            username=self.username,
            id=self.user_id,
            balance=self.balance,
            achievements=None,
            name=first + ' ' + last,
            lang=self.lang
        )


class UserData:
    """
    Class to work with users' data in sql database.
    """
    def __init__(self, env_variables: dict):
        self.auth = env_variables['database_auth'].split(':')
        self.host = env_variables['database']
        # test connection
        self.__connect(self.auth, self.host)

    def get_data(self, query: str, parameters: tuple, cursor=None):
        with self.__connect(self.auth, self.host) as connection:
            with connection.cursor() as cursor:
                cursor.execute(query, parameters)
                result = [r for r in cursor]
        return result

    def insert_data(self, query: str, parameters: tuple):
        with self.__connect(self.auth, self.host) as connection:
            with connection.cursor() as cursor:
                cursor.execute(query, parameters)

    def update_balance(self, user_id: int, balance: float):
        query = 'update casino_user set balance = %s where user_id = %s;'
        with self.__connect(self.auth, self.host) as connection:
            with connection.cursor() as cursor:
                try:
                    updated = cursor.execute(query, (balance, user_id,))
                    log.debug(f'SAVE: {user_id}: {balance}')
                except:
                    updated = None
                    log.error(f'SAVE: Cannot save user {user_id}: {balance}')
        return updated

    @classmethod
    def __connect(cls, auth, host):
        return psycopg2.connect(
            user=auth[0],
            password=auth[1],
            dbname=auth[2],
            host=host,
            sslmode='verify-ca',
            sslrootcert=DATABASE_SERVER_CERT,
            sslcert=DATABASE_CLIENT_CERT,
            sslkey=DATABASE_CLIENT_KEY
        )


def update_session(session_list: List[UserSession], user_id: int,
                   update: dict, session=None):
    if session is None:
        log.debug(f'Updating session for <{user_id}>.')
        session = find_user_session(session_list, user_id)
    for patch in update:
        log.debug(f'Updating session for <{session.user_id}>')
        session.__setattr__(patch, update[patch])


def find_user_session(session_list: List[UserSession], user_id: int):
    for session in session_list:
        if session.user_id == user_id:
            return session
