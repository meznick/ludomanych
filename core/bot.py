# -*- coding: utf-8 -*-
import telebot
from telebot import apihelper
from traceback import print_tb
from logging import getLogger

from core.menu import MainMenu
from core.user import UserData, UserSession
from res.sql import get_user_by_id, create_user


class TelegramBot:
    bot = None

    def __init__(self, env_variables: dict):
        self.log = getLogger('ludomanych')
        self.log.info('Telegram connected to logging!')
        proxy = {'host': env_variables['proxy_host'],
                 'port': env_variables['proxy_port']}
        if proxy:
            try:
                proxy.update({'auth': env_variables['proxy_auth'] + '@'})
                apihelper.proxy = {
                    'https': f"socks5h://{proxy['auth']}{proxy['host']}:{proxy['port']}"
                }
                self.log.info(f"Using proxy: {apihelper.proxy}")
            except TypeError:
                self.log.info(f"Not using proxy")
        self.bot = telebot.AsyncTeleBot(token=env_variables['telegram'])
        me = self.bot.get_me().wait()
        if not issubclass(type(me), telebot.types.User):
            self.log.error('Telegram API was not initiated!')
            raise me[0](print_tb(me[2]))
        self.log.info('Telegram API initiated!')
        self.user_data = UserData(env_variables)
        self.log.info('Database access working!')

    def process_messages(self, msg: telebot.types.Message,
                         session: UserSession, menu: MainMenu):
        if msg.text[0] == '/':
            return self._process_commands(msg, session, menu)

    def handle_first_login(self, msg, context, games):
        # check if user new
        users = self.user_data.get_data(get_user_by_id, (msg.from_user.id,))
        # if user new: add to database
        if len(users) == 0:
            self.user_data.insert_data(create_user, (
                msg.from_user.id, msg.from_user.username,
                msg.from_user.first_name, msg.from_user.last_name,
                msg.from_user.language_code))
            self.log.debug(f'Added 1000 pts to {msg.from_user.id} '
                           f'for reason: registration')
            send_task = self.bot.send_message(
                chat_id=msg.chat.id,
                text="Congratulations, you've been registered and "
                     "received registration bonus of 1000 pts!")
            balance = 1000
        else:
            send_task = None
            balance = float(users[0][1])
        context.append(UserSession(
            user_id=msg.from_user.id,
            games=games,
            username=msg.from_user.username,
            first_name=msg.from_user.first_name,
            last_name=msg.from_user.last_name,
            language=msg.from_user.language_code,
            balance=balance))
        if send_task is not None:
            send_task.wait()

    def _process_commands(self, msg, session, menu):
        if msg.text == '/start':
            return self.bot.send_message(
                chat_id=msg.chat.id,
                text=menu.menus['start_screen'].title.format(user=session),
                reply_markup=menu.menus['start_screen'].keyboard
            )
